<?php
declare(strict_types=1);

include_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use RouteeApp\Exception\InvalidNumberOfLoopsException;
use RouteeApp\Exception\MissingArgumentException;
use RouteeApp\Helpers\ConfigHelper;
use RouteeApp\Helpers\EnvHelper;
use RouteeCom\Request\SendSmsRequest;
use RouteeCom\Response\SendSmsResponse;
use RouteeOwpApi\Entity\CurrentWeatherEntity;
use RouteeOwpApi\Entity\WeatherEntity;
use RouteeOwpApi\Request\CurrentWeatherRequest;

// create a log channel
$log = new Logger('routeeApp');
$log->pushHandler(new StreamHandler('log/routee.log'));
$log->pushHandler(new StreamHandler('php://stdout'));

// load config
ConfigHelper::parseEnv(__DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '.env');

$log->info('Start Routee App');
$log->info('Reading configuration');
$shortopts  = "d::l::"; //d for Delay, l for loops
$options = getopt($shortopts);
$numberOfLoops = (int)EnvHelper::getLoops();
$delay = (int)EnvHelper::getDelay();
$city = EnvHelper::getCity();
$sender = EnvHelper::getSender();
$recipient = EnvHelper::getRecipient();

try {
    if ($numberOfLoops <= 0){
        throw new InvalidNumberOfLoopsException();
    }
    if (empty($city)){
        $city = 'Thessaloniki'; //default
    }
    if (empty($sender)){
        throw new MissingArgumentException("Sender");
    }
    if (empty($recipient)){
        throw new MissingArgumentException("Recipient");
    }
    $currentLoop = 0;
    $log->info("Number of loops: {$numberOfLoops}");
    $log->info("Delay between loops: {$delay} seconds");
    $log->info("City to look up: {$city}");
    $log->info("Sender: {$sender}");
    $log->info("Recipient: {$recipient}");
    while($currentLoop < $numberOfLoops){
        $currentLoop++;
        $log->info("Running loop: {$currentLoop}");
        $log->info("Execute weather request");
        $weatherRequest = new CurrentWeatherRequest();
        $weatherResponse = $weatherRequest->getByCityName($city);
        if ($weatherResponse->isSuccess()){
            $log->info("Weather request executed successfully");
            /**
             * @var CurrentWeatherEntity $weatherEntity
             */
            $weatherEntity = $weatherResponse->toEntity();
            $temperature = $weatherEntity->main->temp;
            $log->info("Temperature in {$city} is {$temperature} celsius");
            $direction = $temperature > 20 ? "more" : "less";
            $body = "Dimitrios Toskas and Temperature {$direction} than 20C. {$temperature}";

            $log->info("Execute send sms request");
            $smsRequest = new SendSmsRequest();
            /**
             * @var SendSmsResponse $sendSmsResponse
             */
            $sendSmsResponse = $smsRequest->send($sender, $recipient, $body);
            if ($sendSmsResponse->isSuccess()){
                $log->info("SMS sent successfully");
            }
            else {
                $log->warning("Send sms request failed");
                $log->warning($sendSmsResponse->getMessage());
            }
        }
        else {
            $log->warning("Weather request failed");
            $log->warning($weatherResponse->getMessage());
        }
        $log->info("End of loop: {$currentLoop}");
        // Bad practice check documentation, section Improvements for explanation
        if($currentLoop < $numberOfLoops){
            sleep($delay);
        }
    }
}
catch (Exception $ex){
    $message = "***Exception***\r\nMessage: {$ex->getMessage()}\r\nCode: {$ex->getCode()}\r\nStackTrace: {$ex->getTraceAsString()}";
    $log->error($message);
}
$log->info("End Routee App");