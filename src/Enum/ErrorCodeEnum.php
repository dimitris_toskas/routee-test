<?php
declare(strict_types=1);

namespace RouteeApp\Enum;

class ErrorCodeEnum
{
    const INVALID_NUMBER_OF_LOOPS = 9000;
    const MISSING_ARGUMENT        = 9001;
}