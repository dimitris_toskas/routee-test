<?php
declare(strict_types=1);

namespace RouteeApp\Enum;

class EnvEnum
{
    const DELAY = 'DELAY';
    const LOOPS = 'LOOPS';
    const CITY  = 'CITY_LOOK_UP';
    const SENDER  = 'SENDER';
    const RECIPIENT  = 'RECIPIENT';
}