<?php
declare(strict_types=1);

namespace RouteeApp\Helpers;

use josegonzalez\Dotenv\Loader;

class ConfigHelper
{
    public static function parseEnv($envLocation)
    {
        if (file_exists($envLocation)) {
            $dotenv = new Loader([$envLocation]);
            $dotenv->parse()
                ->putenv()
                ->toEnv()
                ->toServer();
        }
    }
}