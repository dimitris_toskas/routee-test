<?php
declare(strict_types=1);

namespace RouteeApp\Helpers;

use RouteeApp\Enum\EnvEnum;

class EnvHelper
{
    public static function getDelay():string
    {
        return getenv(EnvEnum::DELAY);
    }
    public static function getLoops():string
    {
        return getenv(EnvEnum::LOOPS);
    }
    public static function getCity():string
    {
        return getenv(EnvEnum::CITY);
    }
    public static function getRecipient():string
    {
        return getenv(EnvEnum::RECIPIENT);
    }
    public static function getSender():string
    {
        return getenv(EnvEnum::SENDER);
    }
}