<?php
declare(strict_types=1);

namespace RouteeApp\Exception;

use RouteeApp\Enum\ErrorCodeEnum;
use Throwable;

class MissingArgumentException extends BaseException
{
    public function __construct($arg = "")
    {
        parent::__construct("Argument {$arg} is missing", ErrorCodeEnum::MISSING_ARGUMENT);
    }
}