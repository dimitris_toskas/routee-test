<?php
declare(strict_types=1);

namespace RouteeApp\Exception;

use RouteeApp\Enum\ErrorCodeEnum;
use Throwable;

class InvalidNumberOfLoopsException extends BaseException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("Invalid number of loops", ErrorCodeEnum::INVALID_NUMBER_OF_LOOPS, $previous);
    }
}