<?php
declare(strict_types=1);

namespace RouteeAppTest\Tests;

use RouteeApp\Helpers\ConfigHelper;

class ConfigHandlerTest extends BaseRouteeAppTest
{
    protected $location = null;
    protected function setUp(): void
    {
        $this->location = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'.env';
        parent::setUp();
    }

    public function testParseEnv()
    {
        $this->assertFalse(isset($_ENV['ROUTEE_API_URL']));
        ConfigHelper::parseEnv($this->location);
        $this->assertTrue(isset($_ENV['ROUTEE_API_URL']));
        $this->assertEquals($_ENV['ROUTEE_API_URL'],'https://connect.routee.net/');
    }
}